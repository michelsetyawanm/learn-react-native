import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';

export default function App() {
  const [name, setName] = useState('Mike');
  const [age, setAge] = useState('20');

  const changeName = (val) => {
    setName(val)
  }
  const changeAge = (val) => {
    setAge(val)
  }

  return (
    <View style={styles.container}>
      {/* Change Name */}
      <Text>Enter Name</Text>
      <TextInput 
        // multiline 
        // secureTextEntry={true}
        // maxLength={3}
        style={styles.input}  
        placeholder='e.g. Mike'
        onChangeText={changeName}
        />

    {/* Change Age */}
    <Text>Enter Age</Text>
      <TextInput
        keyboardType='numeric'
        style={styles.input}  
        placeholder='e.g. 20'
        onChangeText={changeAge}
        />
      <Text>Name: {name}, Age: {age}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    borderWidth: 1,
    borderColor: '#777',
    padding: 8,
    margin: 10,
    width: 200,
    borderRadius: 10,
  }
});
